"""
Module providing the ASGI app for soter-trivy.
"""

import contextlib
import os

from quart import Quart

import httpx

from jsonrpc.model import JsonRpcException

from jsonrpc.server import Dispatcher
from jsonrpc.server.adapter.quart import websocket_blueprint

from .scanner.models import ScannerStatus, ConfigurationIssue


# Build the Quart app
app = Quart(__name__)
# Create the JSON-RPC dispatcher
dispatcher = Dispatcher()
# Register the JSON-RPC blueprint
app.register_blueprint(websocket_blueprint(dispatcher), url_prefix = '/')



# Get configuration from the environment
#: The OPA URL to use
OPA_URL = os.environ['OPA_URL']


@contextlib.asynccontextmanager
async def opa_client():
    """
    Returns an OPA client configured from the environment.
    """
    async with httpx.AsyncClient(base_url = OPA_URL) as client:
        yield client


@dispatcher.register
async def status():
    """
    Return status information for the scanner.
    """
    # Fetch provenance information from OPA
    async with opa_client() as client:
        response = await client.get("/v1/data?provenance=true")
    if response.status_code == 200:
        version = response.json()['provenance']['version']
        available = True
        message = 'available'
    else:
        version = 'unknown'
        available = False
        message = 'not available'
    return ScannerStatus(
        kind = 'Open Policy Agent',
        vendor = 'Open Policy Agent',
        version = version,
        available = available,
        message = message
    )


@dispatcher.register
async def scan_resources(resources):
    """
    Scan the given resources and return any issues.
    """
    # Call OPA with the list of resources
    async with opa_client() as client:
        response = await client.post(
            "/v1/data/soter/resource/issues",
            json = dict(input = dict(resources = resources))
        )
    response.raise_for_status()
    return [
        ConfigurationIssue.parse_obj(issue)
        for issue in response.json().get('result', [])
    ]
